all: typecheck test style flake8 pylint

export PROJECT := tuxbake
TUXPKG_MIN_COVERAGE := 76
TUXPKG_BLACK_OPTIONS := --exclude=tmp/
TUXPKG_MYPY_OPTIONS := --ignore-missing-imports # FIXME while tuxmake doesn't provide typing stubs
TUXPKG_FLAKE8_OPTIONS := --exclude=tmp/

include $(shell tuxpkg get-makefile)

pylint:
	pylint --disable=all --enable=elif,exceptions,stdlib,imports,variables,string,string_constant,logging,newstyle,classes --disable=C0411,C0412,E0401,C0413,E0611,E0202,R0201,R0401,W --enable=R1707,R1714,R1715,R1716,W0235,W0404,W0611,W1401,W1402,W1403,W1505 tuxbake/
